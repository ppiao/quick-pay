var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var merge = require('merge-stream');
var del = require('del');
var uglify = require('gulp-uglify');
var fs = require('fs');
var path = require('path');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');
var babel = require('gulp-babel');


gulp.task('build', function () {
	var s2 =  gulp.src('js/*.js')
		// remove console
		.pipe(babel({
            plugins: ["transform-remove-console"]
        }))
        .pipe(uglify({
	        	mangle: { 
	        		reserved:['require', 'exports', 'module', '$'] 
	        	}
        	}
        ))   
        .pipe(gulp.dest('build/js'));
        
    var s3 = gulp.src('css/*.css')
        .pipe(minifyCSS())
        .pipe(gulp.dest('build/css'));
        
    var s23 = gulp.src('./images/**/*.{png,jpg,jpeg,gif}')
		.pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.jpegtran({progressive: true}),
			imagemin.optipng({optimizationLevel: 5})
		],{verbose: true}))
		.pipe(gulp.dest('build/images'));   
	    
    var s13 = gulp.src(['./*.html'])
    	.pipe(htmlmin({
    		collapseWhitespace: true, 
    		removeComments:true, 
			minifyCSS:true,
			minifyJS:true
    	}))
        .pipe(gulp.dest('./build/'));
    return merge(s2, s3, s23, s13);	    
});

gulp.task('del', function (){
	if(!fs.existsSync(path.join(__dirname, 'build'))){
		fs.mkdirSync('build');
	}
    del.sync('build/');
});


gulp.task('default', ['del', 'build']);
